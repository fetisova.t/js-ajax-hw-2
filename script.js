
const btn = document.querySelector('.main-button');
const container = document.querySelector('.content-wrap');


async function getData(url) {
    const result = await fetch(url);
    const data = await result.json();
    let userIp = data.ip;
    const userInfo = await fetch(`http://ip-api.com/json/${userIp}?lang=ru&fields=1572889`);
    const parsedUserInfo = await userInfo.json();
    const dataList = document.createElement('ul');
    dataList.classList.add('list')
    let items = '';
    for(let key in parsedUserInfo){
        let item = `<li><span class="features">${key}:</span> ${parsedUserInfo[key]}</li>`
        items+=item;
    }
    dataList.innerHTML = items;
    btn.after(dataList);
}
btn.addEventListener('click', function (e) {
    e.preventDefault();
    getData('https://api.ipify.org/?format=json');
});


